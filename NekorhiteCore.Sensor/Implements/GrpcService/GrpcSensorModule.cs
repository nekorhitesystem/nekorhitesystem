﻿using Grpc.Core;
using Grpc.Net.Client;
using NekorhiteCore.Sensor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.Sensor.Implements.GrpcService
{
    internal class GrpcSensorModule : ISensorModule, IDisposable
    {
        private readonly Proto.Sensor.SensorClient client= new(GrpcChannel.ForAddress("http://localhost:65000", new() { Credentials = ChannelCredentials.Insecure}));
        //  new(new Channel("localhost", 7000, ChannelCredentials.Insecure));

        private readonly AsyncServerStreamingCall<Proto.SensorReply> streaming;

        private Proto.SpeedmeterData speed = new();
        private Proto.TachometerData tacho = new();
        private Proto.TempertureData temps = new();
        private Proto.GyroscopeData gyro = new();
        private Proto.AccelerometerData accel = new();
        private Proto.CompassData compass = new();
        private Proto.MagnetometerData magnet = new();
        private Proto.VoltageData voltage = new();

        public GrpcSensorModule(Proto.Sensor.SensorClient client)
        {
            this.client = client;
            streaming = client.ReceiveSensor(new());
        }

        public IObservable<SensorData> GetObservable()
        {
            Task.Factory.StartNew(ReceiveData, TaskCreationOptions.LongRunning);
            return Observable.Interval(TimeSpan.FromMilliseconds(50))
                .Select(GetSensorData)
                .Prepend(SensorData.Default with { Status = State.Found })
                .Finally(Unsubscrive);
        }

        private async Task ReceiveData()
        {
            var reader = streaming.ResponseStream;
            while(await reader.MoveNext())
            {
                speed = reader.Current.Speed ?? speed;
                tacho = reader.Current.Tacho ?? tacho;
                temps = reader.Current.Temps ?? temps;
                gyro = reader.Current.Gyro ?? gyro;
                accel = reader.Current.Accel ?? accel;
                compass = reader.Current.Compass ?? compass;
                magnet = reader.Current.Magnet ?? magnet;
                voltage = reader.Current.Voltage ?? voltage;
            }
        }

        private SensorData GetSensorData(long time)
        {
            return SensorData.Default with
            {
                Status = State.Receive,
                Speed = new ("km/h", speed.Value),
                Tacho = new ("rpm", tacho.Value),
                Tempertures = new("°C", temps.Temp1, temps.Temp2, temps.Temp3, temps.Temp4),
                Accel = new(accel.X, accel.Y, accel.Z),
                Angle = new(gyro.X, gyro.Y, gyro.Z),
                Magnet = new(magnet.X, magnet.Y, magnet.Z),
                Rotate = new(compass.Angle),
                Voltage = new(voltage.Volt1),
            };
        }

        private void Unsubscrive()
        {


        }

        public void Dispose()
        {
            streaming.Dispose();
        }
    }
}
