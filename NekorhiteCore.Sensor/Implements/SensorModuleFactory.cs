﻿using NekorhiteCore.Sensor.Models;
using NekorhiteCore.Sensor.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.Sensor.Implements
{
    internal class SensorModuleFactory : ISensorModuleFactory
    {
        public ISensorModule CreateModule(string portName)
        {
            return new SensorModule(portName);
        }
    }
}
