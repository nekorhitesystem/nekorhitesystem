﻿using Microsoft.Extensions.DependencyInjection;

namespace NekorhiteCore.SensorModule.SerialPort
{
	public static partial class Extensions
	{
		public static IServiceCollection UseSensor(this IServiceCollection service)
		{
			return service
				.AddSingleton<SensorService>()
				.AddTransient<ISensorModuleFactory, SensorModuleFactory>();
		}
	}
}
