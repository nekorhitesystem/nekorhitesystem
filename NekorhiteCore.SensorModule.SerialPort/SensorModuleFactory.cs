﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.SensorModule.SerialPort
{
    internal class SensorModuleFactory : ISensorModuleFactory
    {
        public ISensorModule CreateModule(string portName)
        {
            return new SensorModule(portName);
        }
    }
}
