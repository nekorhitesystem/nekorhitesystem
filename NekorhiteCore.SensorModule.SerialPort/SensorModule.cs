﻿
using NekorhiteCore.SensorModule.Models;
using System.Diagnostics;
using System.Management;
using System.Reactive.Linq;
using System.Text.RegularExpressions;

namespace NekorhiteCore.SensorModule.SerialPort
{
    internal class SensorModule : ISensorModule
    {
        private event Action<SensorData>? SensorEvent;

		private System.IO.Ports.SerialPort? port;
		private DateTime last;

		public string PortName { get; }

		public int Rate { get; }

		public static SensorModule Create(string name)
		{
			return new(name);
		}

		public SensorModule(string name)
		{
			PortName = name;
			Rate = 115200;
			Task.Factory.StartNew(WatchStatus, TaskCreationOptions.LongRunning);
		}

        private async void WatchStatus()
        {
			while (true)
			{   // ポート状態の監視
				if (last.AddMilliseconds(500) < DateTime.Now)
				{   // 500ms以上シリアルポートからの応答が無ければ通信異常として再接続を試行
					try
					{
						if(port is not null)
						{
							SensorEvent?.Invoke(SensorData.Default with { Status = State.NotFound });
						}
						ResetSerialPort();
						SensorEvent?.Invoke(SensorData.Default with { Status = State.Found });
						await Task.Delay(20000);
					}
					catch (InvalidOperationException)
                    {
                        Debug.WriteLine("センサーデバイスが見つかりません");
                        await Task.Delay(1000);
                    }
					catch (Exception ex)
					{
						Debug.WriteLine(ex.Message);
						Debug.WriteLine(ex.StackTrace);
                        await Task.Delay(1000);
                    }
                }
                else
				{
					await Task.Delay(100);
				}
            }
        }

		private void ResetSerialPort()
		{
			port?.Close();
			var portname = Regex.IsMatch(PortName, "^COM\\d+$") ? PortName : GetDevicePort();
			var handshake = System.IO.Ports.Handshake.RequestToSendXOnXOff;
            var p = new System.IO.Ports.SerialPort(portname, Rate) { DtrEnable = true, Handshake = handshake };
			p.DataReceived += DataReceived;
			p.Open();
			port = p;
		}

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Interoperability", "CA1416")]
        private string GetDevicePort()
        {
            var pattern = new Regex(PortName);
            using var entity = new ManagementClass("Win32_PnPEntity");
			using var instances = entity.GetInstances();
            var matches = instances
                .OfType<ManagementObject>()
                .Select(x => x.GetPropertyValue("Name"))
                .Select(x => pattern.Match($"{x}"))
                .Where(x => x.Success)
				.Select(x => x.Value);
			var ports = System.IO.Ports.SerialPort.GetPortNames()
				.Where(x => matches.Any(y => y.Contains($"({x})")));
            return ports.First();
        }

		private void DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
		{
			try
            {
                last = DateTime.Now;
                SensorData? data = port?.ReadLine();
                if (data is not null)
                {
                    SensorEvent?.Invoke(data);
                }
            }
			catch (Exception ex)
			{
				Debug.WriteLine(ex.Message);
				Debug.WriteLine(ex.StackTrace);
			}
		}

		public IObservable<SensorData> GetObservable()
        {
            return Observable.FromEvent<SensorData>(handler => SensorEvent += handler, handler => SensorEvent -= handler);
        }
    }
}
