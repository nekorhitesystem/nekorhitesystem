﻿using System;
using NekorhiteCore.SensorModule.Models;

namespace NekorhiteCore.SensorModule
{
    public interface ISensorModule
    {
        IObservable<SensorData> GetObservable();
    }
}
