﻿using NekorhiteCore.SensorModule;
using NekorhiteCore.SensorModule.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.SensorModule
{
    public class SensorService
	{
		private readonly ConcurrentDictionary<string, ISensorModule> modules = new();

		private readonly ISensorModuleFactory factory;

		public SensorService(ISensorModuleFactory factory)
        {
			this.factory = factory;
        }

		public void SetCalibrateMagnet(double x ,double y, double z)
		{
			SensorData.CalibX = x;
            SensorData.CalibY = y;
            SensorData.CalibZ = z;
        }

		public ISensorModule GetOrCreate(string portName)
		{
			lock(this)
            {
                return modules.GetOrAdd(portName, factory.CreateModule);
            }
		}
	}
}
