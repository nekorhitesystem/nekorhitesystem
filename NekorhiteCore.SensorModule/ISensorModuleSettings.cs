﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.SensorModule
{
    public interface ISensorModuleSettings
    {
        string PortName { get; }

    }

}
