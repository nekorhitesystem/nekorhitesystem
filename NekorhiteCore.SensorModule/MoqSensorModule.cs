﻿using NekorhiteCore.SensorModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.SensorModule
{
#pragma warning disable CS0649
#pragma warning disable IDE0044
#pragma warning disable IDE0052
    internal class MoqSensorModule : ISensorModule
    {
        public MoqSensorModule()
        {
            Task.Factory.StartNew(Loop, TaskCreationOptions.LongRunning);
            Task.Factory.StartNew(Speed, TaskCreationOptions.LongRunning);
            Task.Factory.StartNew(Tacho, TaskCreationOptions.LongRunning);
        }

        private event Action<SensorData>? SensorEvent;

        private double mag;
        private double rad = Math.PI / 180;

        private double accX;
        private double accY;
        private double accZ;

        private double rotX;
        private double rotY;
        private double rotZ;

        private double magX;
        private double magY;
        private double magZ;

        private double temp1;
        private double temp2;
        private double temp3;
        private double temp4;

        private double volt;

        private int state;
        private double speed;
        private double tacho;

        private async Task Loop()
        {
            await Task.Delay(1000);
            SensorEvent?.Invoke(SensorData.Default with { Status = State.NotFound });
            await Task.Delay(1000);
            SensorEvent?.Invoke(SensorData.Default with { Status = State.Found });
            await Task.Delay(1000);
            while (true)
            {
                try
                {
                    await Run();
                }
                catch
                {

                }
            }
        }

        private async Task Speed()
        {
            tacho = 0;
            await Task.Delay(5000);
            while (true)
            {
                while (speed < 30) { speed++; await Task.Delay(55); }
                while (speed > 25) { speed--; await Task.Delay(200); }
                while (speed < 50) { speed++; await Task.Delay(50); }
                while (speed > 45) { speed--; await Task.Delay(200); }
                while (speed < 60) { speed++; await Task.Delay(80); }
                while (speed > 55) { speed--; await Task.Delay(200); }
                while (speed < 75) { speed++; await Task.Delay(120); }
                foreach (var _ in Enumerable.Range(0, 100))
                {
                    speed += RandomNumberGenerator.GetInt32(-1, 2);
                    await Task.Delay(200);
                }
                while (speed > 0) { speed--; await Task.Delay(100); }
                await Task.Delay(3000);
            }
        }

        private async Task Tacho()
        {
            tacho = 0;
            await Task.Delay(5000);
            while (true)
            {
                while (tacho < 800) { tacho += 400; await Task.Delay(50); }
                foreach (var _ in Enumerable.Range(0, 100))
                {
                    tacho += RandomNumberGenerator.GetInt32(-20, 20);
                    await Task.Delay(50);
                }
                while (tacho < 3000) { tacho += 500; await Task.Delay(50); }
                while (tacho > 1000) { tacho -= (int)(tacho / 20); await Task.Delay(50); }
                while (tacho < 3000) { tacho += 500; await Task.Delay(50); }
                while (tacho > 1000) { tacho -= (int)(tacho / 20); await Task.Delay(50); }
                while (tacho < 3000) { tacho += 500; await Task.Delay(50); }
                while (tacho > 850) { tacho -= (int)(tacho / 20); await Task.Delay(50); }
                foreach (var _ in Enumerable.Range(0, 100))
                {
                    tacho += RandomNumberGenerator.GetInt32(-20, 20);
                    await Task.Delay(50);
                }
                while (tacho < 5000) { tacho += 500; await Task.Delay(50); }
                foreach (var _ in Enumerable.Range(0, 100))
                {
                    tacho += RandomNumberGenerator.GetInt32(-20, 20);
                    await Task.Delay(50);
                }
                while (tacho > 850) { tacho -= (int)(tacho / 20); await Task.Delay(50); }
                foreach (var _ in Enumerable.Range(0, 100))
                {
                    tacho += RandomNumberGenerator.GetInt32(-20, 20);
                    await Task.Delay(50);
                }
                while (tacho > 100) { tacho -= 100; await Task.Delay(50); }
                await Task.Delay(1000);
                tacho = 0;
                await Task.Delay(4000);
            }
        }

        private async Task Run()
        {
            SensorData data = SensorData.Default with
            {
                Status = State.Receive,
                Accel = new Vector(accX, accY, accZ),
                Angle = new Vector(rotX, rotY, rotZ),
                Magnet = new Vector(magX, magY, magZ),
                Tempertures = new Temps("°C", temp1, temp2, temp3, temp4),
                Voltage = new Voltage(volt),
                Speed = new Meter("km/h", speed),
                Tacho = new Meter("rpm", tacho),
            };
            if (data is not null)
            {
                SensorEvent?.Invoke(data);
            }

            //speed += (state & 0x01) != 0 ? -04 : 04;
            //tacho += (state & 0x02) != 0 ? -90 : 90;
            accX = Math.Clamp(accX + RandomNumberGenerator.GetInt32(-3, 4) * .01, -.5, .5);
            accY = Math.Clamp(accY + RandomNumberGenerator.GetInt32(-10, 11) * .01, -.5, .5);

            volt = 14.5; ;
            temp1 = 64;
            temp2 = 53;
            temp3 = 34;
            temp4 = 36;

            mag++;
            magX = Math.Sin(mag * rad) * 100;
            magY = Math.Cos(mag * rad) * 100;

            if (speed > 0220) { state |= 0x01; }
            if (tacho > 6000) { state |= 0x02; }
            if (temp1 > 0100) { state |= 0x04; }
            if (temp2 > 0100) { state |= 0x08; }
            if (temp3 > 0100) { state |= 0x10; }
            if (temp4 > 0100) { state |= 0x20; }

            if (speed < 0000) { state &= ~0x01; }
            if (tacho < 0000) { state &= ~0x02; }
            if (temp1 < 0000) { state &= ~0x04; }
            if (temp2 < 0001) { state &= ~0x08; }
            if (temp3 < 0002) { state &= ~0x10; }
            if (temp4 < 0003) { state &= ~0x20; }

            if (mag > +720) { state |= +0x40; }
            if (mag < -360) { state &= ~0x40; }

            await Task.Delay(50);
        }


        public IObservable<SensorData> GetObservable()
        {
            return Observable.FromEvent<SensorData>(handler => SensorEvent += handler, handler => SensorEvent -= handler);
        }
    }
#pragma warning restore CS0649
#pragma warning restore IDE0044
#pragma warning restore IDE0052
}
