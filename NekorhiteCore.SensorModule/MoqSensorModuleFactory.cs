﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.SensorModule
{
    internal class MoqSensorModuleFactory : ISensorModuleFactory
    {
        public ISensorModule CreateModule(string portName)
        {
            return new MoqSensorModule();
        }
    }
}
