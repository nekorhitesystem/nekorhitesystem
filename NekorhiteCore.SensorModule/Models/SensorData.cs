﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.SensorModule.Models
{
    public record SensorData(string Status, Meter Speed, Meter Tacho, Temps Tempertures, Vector Accel, Vector Angle, Vector Magnet, Compass Rotate, Voltage Voltage)
    {
        private const double accX = 0;
        private const double accY = 0;
        private const double accZ = 0;

        public static double CalibX { get; set; } = -22.0;
        public static double CalibY { get; set; } = -28.1;
        public static double CalibZ { get; set; } = +37.2;

        public static SensorData Default => new(State.Initial, Meter, Meter, Temps, Vector, Vector, Vector, Compass, Volt);
        private static Meter Meter => new("", 0);
        private static Temps Temps => new("", 0, 0, 0, 0);
        private static Vector Vector => new(0, 0, 0);
        private static Voltage Volt => new(0);
        private static Compass Compass => new(0);

        public static implicit operator SensorData?(string? line)
        {
            var args = line?.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            //File.AppendAllText("calib.csv", string.Join(",", args!) + "\n");
            if (args is null || args.Length <= 16) { return null; }
            Meter speed = new("kph", double.TryParse(args[0], out var s) && s != 0 ? 160000 / s : 0);
            Meter tacho = new("rpm", double.TryParse(args[1], out var t) && t != 0 ? 4000000 / t : 0);
            Temps temps = new("°C", args[2].ToDouble(0), args[3].ToDouble(0), args[4].ToDouble(0), args[5].ToDouble(0));
            Vector acc = new(args[07].ToDouble(0) / 9.8 + accX, -args[08].ToDouble(0) / 9.8 + accY, args[09].ToDouble(0) / 9.8 + accZ);
            Vector rot = new(args[10].ToDouble(0), args[11].ToDouble(0), args[12].ToDouble(0));
            Vector mag = new(args[13].ToDouble(0) + CalibX, args[14].ToDouble(0) + CalibY, args[15].ToDouble(0) + CalibZ);
            Compass com = new(Math.Atan2(mag.X, mag.Y) * 180 / Math.PI);
            Voltage volt = new(args[16].ToDouble(0));
            return new(State.Receive, speed, tacho, temps, acc, rot, mag, com, volt);
        }
    }

    public static class State
    {
        public const string Initial = nameof(Initial);
        public const string Found = nameof(Found);
        public const string Receive = nameof(Receive);
        public const string NotFound = nameof(NotFound);
    }

    public record Meter(string Unit, double Value);

    public record Temps(string Unit, double Temp1, double Temp2, double Temp3, double Temp4);

    public record Vector(double X, double Y, double Z);

    public record Compass(double Roll);

    public record Voltage(double V1);
}
