﻿using Microsoft.Extensions.DependencyInjection;

namespace NekorhiteCore.SensorModule
{
	public static partial class Extensions
	{
		public static IServiceCollection UseMoqSensor(this IServiceCollection service)
		{
			return service
				.AddSingleton<SensorService>()
				.AddTransient<ISensorModuleFactory, MoqSensorModuleFactory>();
        }

        internal static double ToDouble(this string str, double def)
        {
            return double.TryParse(str, out double val) ? val : def;
        }
    }
}
