﻿using NekorhiteCore.VideoStreaming;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NekorhiteCore.VideoStreaming.OpenCV
{
    internal class CaptureStreamingModule : IStreamingModule, IDisposable
    {
        private readonly StreamingModuleConfig config;
        private readonly Mat mat = new();
        private readonly SemaphoreSlim semaphore = new(0);
        private readonly VideoCapture capture = new();
        private bool disposedValue;
        private bool IsConnected;
        private bool updated;

        public CaptureStreamingModule(StreamingModuleConfig config)
        {
            this.config = config;
            Task.Factory.StartNew(CaptureTask, TaskCreationOptions.LongRunning);
        }

        private void CaptureTask()
        {
            while (!disposedValue)
            {
                IsConnected = IsConnected && capture.Grab();
                if (!IsConnected)
                {
                    try
                    {
                        if (config.Rtsp)
                        {
                            IsConnected = capture.Open(config.Address);
                        }
                        else
                        {
                            IsConnected = capture.Open(config.Device, VideoCaptureAPIs.DSHOW);
                            capture.FrameWidth = config.CaptureSize.Width;
                            capture.FrameHeight = config.CaptureSize.Height;
                            capture.FourCC = "MJPG";
                            capture.ConvertRgb = false;
                        }
                    }
                    catch
                    {
                        IsConnected = false;
                    }
                }
                updated = true;
                Thread.Sleep(1);
            }
        }

        public Task UpdateAsync(Stream stream)
        {
            UpdateInternal(stream);
            return Task.CompletedTask;
        }

        private void UpdateInternal(Stream stream)
        {
            if (IsConnected && updated)
            {
                capture.Retrieve(mat);
                if (!mat.Empty())
                {
                    if (config.CaptureSize.Width != capture.FrameWidth)
                    {
                        var mul = (double)config.VideoSize.Width / mat.Width;
                        Cv2.Resize(mat, mat, new Size((int)(mat.Width * mul), (int)(mat.Height * mul)), interpolation: InterpolationFlags.Nearest);
                    }
                    mat.WriteToStream(stream, ".jpg", new ImageEncodingParam(ImwriteFlags.JpegQuality, config.Quality));
                }
                updated = false;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    mat.Dispose();
                }
                disposedValue = true;
                while (capture != null) Thread.Yield();
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
