﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NekorhiteCore.VideoStreaming;
using NekorhiteCore.VideoStreaming.OpenCV;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.VideoStreaming
{
	public static partial class Extensions
	{
		public static IServiceCollection UseRtspStreaming(this IServiceCollection service, IConfiguration configration)
		{
			return service
				.Configure <StreamingModuleConfig>(configration)
				.AddTransient<IStreamingModuleFactory, CaptureStreamingModuleFactory>();
		}
	}
}
