﻿using Microsoft.Extensions.Options;
using NekorhiteCore.VideoStreaming;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.VideoStreaming.OpenCV
{
    internal class CaptureStreamingModuleFactory : IStreamingModuleFactory
    {
        public readonly StreamingModuleConfig config;

        public CaptureStreamingModuleFactory(IOptions<StreamingModuleConfig> options)
        {
            config = options.Value;
        }

        public IStreamingModule CreateModule(string _)
        {
            return new CaptureStreamingModule(config);
        }
    }
}
