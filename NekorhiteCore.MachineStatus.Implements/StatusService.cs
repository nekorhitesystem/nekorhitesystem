﻿using Grpc.Core;
using Grpc.Net.Client;
using MagicOnion.Client;
using Microsoft.Extensions.Options;
using NekorhiteCore.MachineStatus.Services;

namespace NekorhiteCore.MachineStatus.Client
{
    internal class StatusService : IStatusService
    {
        private readonly GrpcChannel channel;
        private readonly IStatusModuleService service;

        public StatusService(IOptions<StatusModuleSettings> settings)
        {
            channel = GrpcChannel.ForAddress(settings.Value.Address, new() { Credentials = ChannelCredentials.Insecure });
            service = MagicOnionClient.Create<IStatusModuleService>(channel);
        }

        public async Task<StatusModel> GetStatusAsync()
        {
            return new(await service.GetCpuPercentAsync(), await service.GetMemoryPercentAsync());
        }
    }
}
