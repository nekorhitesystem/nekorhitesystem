﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NekorhiteCore.MachineStatus.Client;

namespace NekorhiteCore.MachineStatus.Implements
{
	public static partial class Extensions
	{
		public static IServiceCollection UseMachineStatus(this IServiceCollection service, IConfiguration settings)
		{
			return service
				.Configure<StatusModuleSettings>(settings)
				.AddTransient<IStatusService, StatusService>();
		}
	}
}
