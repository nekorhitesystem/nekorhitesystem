using NekorhiteCore.MachineStatus.Implements;
using NekorhiteCore.SensorModule;
using NekorhiteCore.SensorModule.MagicOnion;
using NekorhiteCore.SensorModule.SerialPort;
using NekorhiteCore.VideoStreaming;
using NekorhiteTourersSystem.Settings;
using MagicOnion.Client;
using NekorhiteCore.Messages;
using Grpc.Core;
using Grpc.Net.Client;
using Grpc.Net.ClientFactory;
using Microsoft.Extensions.Options;
using NekorhiteCore.MachineStatus.Client;

var builder = WebApplication.CreateBuilder(args);
var streamconfig = builder.Configuration.GetSection("StreamSettings");
var sensorconfig = builder.Configuration.GetSection("SensorSettings");
var statusconfig = builder.Configuration.GetSection("StatusSettings");
// Add services to the container.
builder.Services.AddControllersWithViews();

if(sensorconfig.GetValue<bool>("MoqModule"))
{
    builder.Services.UseMoqSensor();
}
else if (sensorconfig.GetValue<bool>("GrpcModule"))
{
    builder.Services.UseGrpcSensor();
}
else
{
    builder.Services.UseSensor();
}

builder.Services.AddGrpcClient<INekorhiteService>(options =>
{
    options.Address = new Uri(statusconfig.GetValue<string>("Address"));
    options.ChannelOptionsActions.Add(x => x.Credentials = ChannelCredentials.Insecure);
    options.Creator = MagicOnionClient.Create<INekorhiteService>;
});

builder.Services.EnableStreamingService();
builder.Services.UseRtspStreaming(streamconfig);
builder.Services.UseMachineStatus(statusconfig);
builder.Services.Configure<SensorSetting>(sensorconfig);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
}

app.UseStaticFiles();
app.UseRouting();

app.UseWebSockets();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html"); ;

app.Run();
