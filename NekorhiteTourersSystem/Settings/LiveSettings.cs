﻿namespace NekorhiteTourersSystem.Settings
{
    public class LiveSettings
    {
        public string? Path { get; set; }

        public short Port { get; set; }
    }
}
