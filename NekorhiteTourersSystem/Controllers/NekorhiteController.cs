﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NekorhiteCore.SensorModule.Models;
using NekorhiteTourersSystem.Settings;
using System.Diagnostics;
using System.IO.Pipelines;
using System.Net.WebSockets;
using System.Text.Json;
using NekorhiteCore.SensorModule;
using NekorhiteCore.Messages;
using MessagePack.Formatters;

namespace NekorhiteTourersSystem.Controllers
{
	[Route("api")]
	[ApiController]
	public class NekorhiteController : ControllerBase
	{
		private readonly SensorService service1;
		private readonly INekorhiteService service;
		private readonly SensorSetting setting;

		public NekorhiteController(SensorService service, INekorhiteService nekorhite, IOptions<SensorSetting> settings)
		{
			this.service1 = service;
			this.service = nekorhite;
			setting = settings.Value;
		}

		[HttpGet("status")]
		public async Task Status(CancellationToken token)
		{
			var sensor = service1.GetOrCreate(setting.PortName);
			using var streaming = HttpContext.WebSockets.IsWebSocketRequest ?
				new Streaming(await HttpContext.WebSockets.AcceptWebSocketAsync()) :
				new Streaming(Response.BodyWriter);
			await streaming.WriteAsync<SensorModel>(new(SensorData.Default));
            using var subscribe = sensor.GetObservable().Subscribe(async data =>
            {
                try { await streaming.WriteAsync<SensorModel>(new(data)); } catch { }
            });
            while (!token.IsCancellationRequested)
            {
                await Task.Delay(1000, token);
            }
		}

		[HttpGet("title")]
		public async Task<TitleModel> GetTitle()
		{
			var setting = await service.GetSettingsAsync();
			return new(setting.Title);
        }

        [HttpPost("title")]
        public async Task SetTitle(TitleModel model)
        {
            var setting = await service.GetSettingsAsync();
			await service.SetSettingsAsync(setting with { Title = model.Title });
        }

        [HttpGet("caribration")]
        public async Task<CaribrationModel> GetCaribration()
        {
            var setting = await service.GetSettingsAsync();
            return new(setting.MagX, setting.MagY, setting.MagZ);
        }

        [HttpPost("caribration")]
		public async Task SetCaribration(CaribrationModel model)
        {
            var setting = await service.GetSettingsAsync();
            await service.SetSettingsAsync(setting with { MagX = model.X, MagY = model.Y, MagZ = model.Z });
        }

        private class Streaming : IDisposable
        {
            private readonly WebSocket? socket;
            private readonly PipeWriter? pipe;

            public Streaming(WebSocket socket) => this.socket = socket;
            public Streaming(PipeWriter pipe) => this.pipe = pipe;

            public async Task WriteAsync<T>(T data)
            {
                var task = socket?.SendAsync(data) ?? pipe?.WriteAsync(data) ?? Task.CompletedTask;
                await task;
            }
            public void Dispose()
            {
                socket?.Dispose();
            }
        }
    }

	public record SensorModel : SensorData
	{
		public DateTime TimeStamp { get; init; }

		public string Time => $"{TimeStamp:yyyy/MM/dd HH:mm:ss}";

		public SensorModel(SensorData data) : base(data.Status, data.Speed, data.Tacho, data.Tempertures, data.Accel, data.Angle, data.Magnet, data.Rotate, data.Voltage)
		{
			TimeStamp = DateTime.Now;
        }

    }

    public record TitleModel(string? Title);

	public record CaribrationModel(double X, double Y, double Z);

	internal static partial class Extensions
	{
		private static readonly JsonSerializerOptions option = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };
		public static Task SendAsync<T>(this WebSocket socket, T data)
		{
			var message = JsonSerializer.SerializeToUtf8Bytes(data, option);
			return socket.SendAsync(message, WebSocketMessageType.Text, true, default);
		}

		public static async Task WriteAsync<T>(this PipeWriter writer, T data)
		{
			var message = JsonSerializer.SerializeToUtf8Bytes(data, option);
			await writer.WriteAsync(message, default);
		}
	}
}