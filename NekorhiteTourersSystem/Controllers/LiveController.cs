﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NekorhiteCore.VideoStreaming;
using NekorhiteTourersSystem.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteTourersSystem.Controllers
{
    [Route("live")]
    public class LiveController : ControllerBase
    {
        private readonly LiveSettings settings;

        private readonly StreamingService streamingService;
        public LiveController(IOptions<LiveSettings> option, StreamingService streamingService)
        {
            settings = option.Value;
            this.streamingService = streamingService;
        }

        [HttpGet("port")]
        public IActionResult Port()
        {
            return Ok(new { settings.Port });
        }

        [HttpGet("front.mjpeg")]
        public async Task MjpegFront(CancellationToken token)
        {
            var boundary = RandomNumberGenerator.GetInt32(int.MaxValue).ToString();
            Response.ContentType = $"multipart/x-mixed-replace;boundary={boundary}";
            Response.StatusCode = StatusCodes.Status200OK;
            await Response.BodyWriter.FlushAsync();
            var camera = streamingService.GetOrCreateFrontCamera();
            using var memory = new MemoryStream();
            while (!token.IsCancellationRequested)
            {
                await camera.UpdateAsync(memory);
                if (memory.Position == 0)
                {
                    await Task.Delay(10);
                    continue;
                }
                var data = memory.GetBuffer().AsMemory(..(int)memory.Position);

                await Response.BodyWriter.WriteLineAsync($"--{boundary}");
                await Response.BodyWriter.WriteLineAsync($"Content-Type: image/jpeg");
                await Response.BodyWriter.WriteLineAsync($"Content-Length: {data.Length}");
                await Response.BodyWriter.WriteLineAsync($"");
                await Response.BodyWriter.WriteAsync(data, CancellationToken.None);
                await Response.BodyWriter.FlushAsync();
                memory.SetLength(0);
            }
        }

        [HttpGet("front.m3u8")]
        public IActionResult Front()
        {
            FileInfo info = new(Path.Combine(settings.Path!, "front.m3u8"));
            return info.Exists ? File(info.OpenRead(), "application/vnd.apple.mpegurl") : NotFound();
        }

        [HttpGet("front-{index}.ts")]
        public IActionResult Front(string index)
        {
            FileInfo info = new(Path.Combine(settings.Path!, $"front-{index}.ts"));
            return info.Exists ? File(info.OpenRead(), "video/mp2t") : NotFound();
        }

        [HttpGet("rear")]
        public IActionResult Rear()
        {
            FileInfo info = new(Path.Combine(settings.Path!, "rear/rear.m3u8"));
            return info.Exists ? File(info.OpenRead(), "application/vnd.apple.mpegurl") : NotFound();
        }

        [HttpGet("rear-{index}.ts")]
        public IActionResult Rear(string index)
        {
            FileInfo info = new(Path.Combine(settings.Path!, $"rear/rear-{index}.ts"));
            return info.Exists ? File(info.OpenRead(), "video/mp2t") : NotFound();
        }
    }

    internal static partial class PipeWriterExtensions
    {
        public static async Task WriteLineAsync(this System.IO.Pipelines.PipeWriter writer, string line = "")
        {
            await writer.WriteAsync(Encoding.UTF8.GetBytes(line + Environment.NewLine));
        }
    }
}
