import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { Awaitable, SensorData, SensorService } from '../sensor/sensor.service';

@Component({
  selector: 'splash-view',
  templateUrl: './splash.component.html',
  styleUrls: [ './splash.component.scss' ]
})
export class SplashComponent {
  isLoaded = false;
  constructor(sensor: SensorService, private router: Router) {
    sensor.data.subscribe(data => this.onNext(data));
  }

  async onNext(data: SensorData) {
    await Awaitable.delay(1000);
    this.isLoaded = true;
  }
}
