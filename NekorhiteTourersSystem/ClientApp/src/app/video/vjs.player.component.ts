//import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
//import videojs from 'video.js';
//
//
//@Component({
//  selector: 'app-vjs-player',
//  template: `
//      <video #target class="video-js" muted preload="none" ></video>
//  `,
//  styleUrls: [
//    './vjs.player.component.css'
//  ],
//  encapsulation: ViewEncapsulation.None,
//})
//export class VjsPlayerComponent implements OnInit, OnDestroy {
//  @ViewChild('target', { static: true }) target: ElementRef | undefined;
//  // see options: https://github.com/videojs/video.js/blob/maintutorial-options.html
//  @Input() options?: {
//    autoplay: boolean,
//    controls: true,
//    sources: {
//      src: string,
//      type?: string,
//    }[],
//    fill: boolean,
//  };
//  player: videojs.Player | null = null;
//
//  constructor(private elementRef: ElementRef) {
//  }
//
//  async ngOnInit() {
//    this.player = videojs(this.target!.nativeElement, this.options, function onPlayerReady() {
//      console.log('onPlayerReady', this);
//    });
//    await new Promise(resolve => setTimeout(resolve, 5000));
//    this.player.currentTime(this.player.bufferedEnd() - 1);
//    while (true) {
//      await new Promise(resolve => setTimeout(resolve, 10000));
//
//    }
//  }
//
//  ngOnDestroy() {
//    if (this.player) {
//      this.player.dispose();
//    }
//  }
//}
//
