import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Awaitable, SensorService } from '../sensor/sensor.service';

@Component({
  selector: 'menu-control',
  templateUrl: './menu.component.html',
  styleUrls: [ './menu.component.scss' ]
})
export class MenuComponent implements OnInit {
  isExpanded = false;
  port = 80;
  address = "";
  addressList: Array<string> = [];
  status: Status | undefined;

  debug = false;
  constructor(private http: HttpClient, private sensor: SensorService) {
    this.debug = this.sensor.debug;
  }

  async ngOnInit() {
    this.addressList = await this.http.get<Address>("info/address").toPromise().then(x => x.addressList);
    this.address = this.addressList[0];
    this.addressList = this.addressList.slice(1);
    this.port = await this.http.get<Live>("live/port").toPromise().then(x => x.port);
    while (true) {
      this.status = await this.http.get<Status>("info/status").toPromise();
      await Awaitable.delay(1000);
    }
  }

  openMenu() {
    this.isExpanded = true;
  }

  closeMenu() {
    this.isExpanded = false;
  }

  toggleMenu() {
    this.isExpanded = !this.isExpanded;
  }

  toggleDebug() {
    this.sensor.debug = this.debug = !this.sensor.debug;
  }

  startCaribration() {
    this.sensor.isVisible = true;
    //this.sensor.startCalibration();
  }
}

interface Address {
  addressList: Array<string>;
}

interface Live {
  port: number;
}

interface Status {
  cpuAverage: number;
  memoryUsed: number;
}
