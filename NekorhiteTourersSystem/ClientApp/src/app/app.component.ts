import { Component, OnInit } from '@angular/core';
import { Awaitable, SensorService } from './sensor/sensor.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent {
  title = 'app';
  isLoaded = false;
  loadCompleted = false;
  constructor(sensor: SensorService) {
    sensor.data.subscribe(x => this.onReceived());
  }

  async onReceived() {
    this.isLoaded = true;
    await Awaitable.delay(2000);
    this.loadCompleted = true;
  }
}
