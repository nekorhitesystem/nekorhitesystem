import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { SplashComponent } from './splash/splash.component';
import { AccelerateControl } from './sensor/accelerate/control';
import { AngularControl } from './sensor/angularvec/control';
import { CompassControl } from './sensor/compass/control';
import { SpeedMeterControl } from './sensor/speedmeter/control';
import { TachoMeterControl } from './sensor/tachometer/control';
import { TemperturesControl } from './sensor/tempertures/control';
import { VoltagesControl } from './sensor/voltages/control';
import { ViewComponent } from './view/view.component';
//import { VjsPlayerComponent } from './video/vjs.player.component';
import { MenuComponent } from './menu/menu.component';
import { DebugComponent } from './debug/debug.component';
import { CaribrationComponent } from './caribration/caribration.component';

@NgModule({
  declarations: [
    AppComponent,
    SplashComponent,
    ViewComponent,
    AccelerateControl,
    MenuComponent,
    DebugComponent,
    CaribrationComponent,
    AngularControl,
    CompassControl,
    SpeedMeterControl,
    TachoMeterControl,
    TemperturesControl,
    VoltagesControl,
//    VjsPlayerComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: ViewComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
