import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Awaitable, SensorData, SensorService } from '../sensor/sensor.service';

@Component({
  selector: 'app-home',
  templateUrl: './view.component.html',
  styleUrls: [ './view.component.scss' ]
})
export class ViewComponent {
  data: SensorData | null = null;
  title: string = "TOURER'S RECORD SYSTEM";
  now = new Date;
  front: string = "";
  rear: string = "";
  isLoaded: boolean = false;
  isReady: boolean = false;
  videoReady: boolean = false;
  debug = false;
  calib = false;

  constructor(private sensor: SensorService, private http: HttpClient, private router: ActivatedRoute) {
    this.debug = sensor.debug;
    sensor.data.subscribe(data => this.onNext(data));
    setInterval(() => this.now = new Date, 1000);
    console.debug(this.router.snapshot.queryParams);
  }

  async ngOnInit() {
    this.title = await this.http.get<TitleModel>("api/title").toPromise().then(x => x.title) ?? this.title;
    await new Promise(resolve => this.sensor.data.subscribe(resolve));
    await Awaitable.delay(500);
    this.isLoaded = true;
    this.isReady = true;
    await Awaitable.delay(1500);
    this.videoReady = true;
    this.sensor.connected.subscribe(data => this.videoReady = true);
    this.sensor.connecting.subscribe(data => this.videoReady = false);

    while (true) {
      await Awaitable.delay(30);
      this.debug = this.sensor.debug;
    }
  }

  onVideoReady() {
    this.videoReady = true;
    this.front = "live/front.m3u8";
  }

  onVideoError() {
    this.videoReady = false;
    this.front = ""
  }

  onNext(data: SensorData) {
    this.data = data;
  }

  changeTitle() {
    const data = prompt() ?? this.title;
    this.title = data == "" ? "TOURER'S RECORD SYSTEM" : data;
    this.http.post("api/title", { title: this.title }).subscribe();
  }
}

interface TitleModel {
  title: string
}
