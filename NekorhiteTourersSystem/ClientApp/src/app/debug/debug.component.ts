import { Component } from '@angular/core';
import { Awaitable, SensorData, SensorService } from '../sensor/sensor.service';

@Component({
  selector: 'debug-control',
  templateUrl: './debug.component.html',
  styleUrls: [ './debug.component.scss' ]
})
export class DebugComponent {
  data: SensorData | null = null;
  isLoaded: boolean = false;
  isReady: boolean = false;
  debug = false;

  constructor(private sensor: SensorService) {
    sensor.data.subscribe(data => this.onNext(data));
  }


  async ngOnInit() {
    while (true) {
      this.debug = this.sensor.debug;
      await Awaitable.delay(30);
    }
  }

  onNext(data: SensorData) {
    this.data = data;
  }
}
