import { Component } from '@angular/core';
import { ControlBase } from '../control.base';
import { Awaitable, SensorData } from '../sensor.service';

@Component({
  selector: 'speedmeter-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class SpeedMeterControl extends ControlBase {

  ready = false;
  async onInitialized() {
    const indicator = document.getElementById("speedindicator")!;
    await Awaitable.delay(2500);
    this.setValue(220);
    indicator.style.transition = `transform 0.5s ease`;
    await Awaitable.delay(1000);
    this.setValue(0);
    indicator.style.transition = `transform 0.5s ease`;
  }

  onNext(data: SensorData) {
    this.setValue(data.speed.value);
  }

  setValue(value: number) {
    const indicator = document.getElementById("speedindicator")!;
    indicator.style.transform = `rotateZ(${Math.min(230, value) - 110}deg)`;
    indicator.style.transition = `transform 0.5s ease`;
  }
}
