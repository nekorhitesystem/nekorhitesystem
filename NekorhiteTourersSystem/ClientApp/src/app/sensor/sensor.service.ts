import { HttpClient } from "@angular/common/http";
import { Inject, Injectable } from "@angular/core";
import { Observable, Observer, Subscriber } from "rxjs";
import { filter, map, share } from "rxjs/operators"
import { webSocket } from "rxjs/webSocket";

@Injectable({
    providedIn: "root"
})
export class SensorService {
  private readonly sensor: Observable<SensorData>;

  private _debug = document.cookie.split("; ").find(x => x.startsWith("debug="))?.substring(6) == "true" ?? false;
  get debug() {
    return this._debug; 
  }
  set debug(value: boolean) {
    document.cookie = `debug=${this._debug = value}`;
  }

  isVisible = false;

  get data() { return this.sensor.pipe(filter(x => x.status === 'Receive')); }


  get connecting() { return this.sensor.pipe(filter(x => x.status === 'NotFound')); }
  get connected() { return this.sensor.pipe(filter(x => x.status === 'Found')); }

  readonly acceleration: Observable<Vector3d>;
  readonly rotationrate: Observable<Vector3d>;
  readonly orientations: Observable<Vector3d>;

  readonly speedmeter: Observable<Meter>;
  readonly tachometer: Observable<Meter>;
  readonly temperture: Observable<Tempertures>;

  readonly voltage: Observable<Voltage>;

  private calibdata: Vector3d | undefined;

  constructor(@Inject('BASE_URL') private baseUrl: string, private http: HttpClient) {
    this.sensor = new Observable(this.connection).pipe(share());
    this.connecting.subscribe(x => console.log("connecting"), x => console.log(x));
    this.connected.subscribe(x => console.log("connected"), x => console.log(x));

    this.acceleration = this.data.pipe(map(x => x.accel));
    this.rotationrate = this.data.pipe(map(x => x.angle));
    this.orientations = this.data.pipe(map(x => x.magnet));

    this.speedmeter = this.data.pipe(map(x => x.speed));
    this.tachometer = this.data.pipe(map(x => x.tacho));
    this.temperture = this.data.pipe(map(x => x.tempertures));

    this.voltage = this.data.pipe(map(x => x.voltage));
  }

  connection = (observer: Observer<SensorData>) => {
    const socket = webSocket<SensorData>(this.baseUrl.replace("http", "ws") + "api/status/");
    socket.subscribe(x => observer.next(x), x => this.error(observer));
  }

  async getCaribration(): Promise<Vector3d> {
    this.calibdata ??= await this.http.get<Vector3d>("api/caribration").toPromise();
    return this.calibdata;
  }

  async setCaribration(value: Vector3d): Promise<void> {
    this.calibdata = value;
    this.http.post("api/caribration", value).subscribe();
  }

  error = (observer: Observer<SensorData>) => {
    observer.next({
      status: "NotFound",
      time: "",
      accel: { x: 0, y: 0, z: 0 },
      magnet: { x: 0, y: 0, z: 0 },
      angle: { x: 0, y: 0, z: 0 },
      rotate: { roll: 0 },
      speed: { unit: "", value: 0 },
      tacho: { unit: "", value: 0 },
      tempertures: { temp1: 0, temp2: 0, temp3: 0, temp4: 0, unit: "" },
      voltage: { v1:0 }
    });
    this.connection(observer);
  }
}

export interface SensorData {
  readonly time: string;
  readonly status: string;
  readonly accel: Vector3d;
  readonly magnet: Vector3d;
  readonly angle: Vector3d;
  readonly rotate: Compass;
  readonly speed: Meter;
  readonly tacho: Meter;
  readonly tempertures: Tempertures;
  readonly voltage: Voltage;
}

export interface Vector3d {
  readonly x: number;
  readonly y: number;
  readonly z: number;
}

export interface Compass {
  readonly roll: number;
}

export interface Meter {
  readonly unit: string;
  readonly value: number;
}

export interface Tempertures {
  readonly unit: string;
  readonly temp1: number;
  readonly temp2: number;
  readonly temp3: number;
  readonly temp4: number;
}

export interface Voltage {
  readonly v1: number;
}

export class Awaitable {

  static delay = (time: number) => {
    return new Promise(resolve => setTimeout(resolve, time))
  }

  static runAsync = (func: () => void) => {
   return new Promise(resolve => resolve(func()))
  }
}
