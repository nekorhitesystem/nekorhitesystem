import { Component } from '@angular/core';
import { ControlBase } from '../control.base';
import { SensorData, Tempertures } from '../sensor.service';

@Component({
  selector: 'tempertures-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class TemperturesControl extends ControlBase {
  unit: string = "°C";
  temp1: number = 0;
  temp2: number = 0;
  temp3: number = 0;
  temp4: number = 0;
  count = 0;

  onNext(data: SensorData) {
    this.setValue(data.tempertures);
  }

  setValue(data: Tempertures) {
    if (this.count < 50) { this.count++; }
    this.unit = data.unit;
    this.temp1 += (data.temp1 - this.temp1) / (this.count < 50 ? 10 : 100);
    this.temp2 += (data.temp2 - this.temp2) / (this.count < 50 ? 10 : 100);
    this.temp3 += (data.temp3 - this.temp3) / (this.count < 50 ? 10 : 100);
    this.temp4 += (data.temp4 - this.temp4) / (this.count < 50 ? 10 : 100);
  }
}
