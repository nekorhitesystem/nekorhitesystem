import { Directive } from "@angular/core";
import { Awaitable, SensorData, SensorService } from "./sensor.service";

@Directive()
export abstract class ControlBase {
  isInitialized = false;
  isLoaded = false;
  isReady = false;

  protected onInitialized() { }
  abstract onNext(data: SensorData): void

  constructor(sensor: SensorService) {
    sensor.data.subscribe(x => this.next(x))
  }

  private next(data: SensorData) {
    if (!this.isInitialized) {
      this.init()
    } else if (this.isReady) {
      this.onNext(data)
    }
  }

  private async init() {
    this.isInitialized = true;
    await Awaitable.delay(1000)
    this.isLoaded = true
    this.onInitialized()
    await Awaitable.delay(5000)
    this.isReady = true
  }
}
