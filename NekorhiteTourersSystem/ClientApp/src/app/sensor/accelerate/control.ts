import { Component } from '@angular/core';
import { ControlBase } from '../control.base';
import { Awaitable, SensorData, SensorService, Vector3d } from '../sensor.service';

@Component({
  selector: 'accelerate-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class AccelerateControl extends ControlBase {
  private calx: number = 0;
  private caly: number = 0;
  private accx: number = 0;
  private accy: number = 0;

  private isFirst = true;

  onNext(data: SensorData) {
    if (this.isFirst) {
      this.accx = data.accel.y;
      this.accy = data.accel.x;
      this.isFirst = false;
    } else {
      this.setValue(data.accel);
    }
  }

  setValue(data: Vector3d) {
    const x = data.y;
    const y = data.x;
    this.calx += (x - this.calx) / 100;
    this.caly += (y - this.caly) / 100;
    this.accx += (x - this.accx) / 10;
    this.accy += (y - this.accy) / 10;
    const point = document.getElementById("accelpoint")!;
    point.style.left = `${50 + (this.accx - this.calx) * 100}%`;
    point.style.top = `${50 + (this.accy - this.caly) * 100}%`;
  }
}
