import { Component, OnInit } from '@angular/core';
import { ControlBase } from '../control.base';
import { Awaitable, Compass, SensorData, SensorService, Vector3d } from '../sensor.service';

@Component({
  selector: 'compass-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class CompassControl extends ControlBase implements OnInit {
  magx = 100;
  magy = 0;
  rot = 0;

  calibx = 0;
  caliby = 0;

  constructor(private sensor: SensorService) {
    super(sensor)
  }

  async ngOnInit() {
    while (true) {
      const data = await this.sensor.getCaribration()
      this.calibx = data.x
      this.caliby = data.y
      await Awaitable.delay(1000);
    }
  }

  onNext(data: SensorData) {
    this.setValue(data.magnet, data.rotate.roll);
  }

  setValue(data: Vector3d, roll: number) {
    this.magx += (data.x - this.magx + this.calibx) / 10;
    this.magy += (data.y - this.magy + this.caliby) / 10;
    //this.rot += (data.roll - this.rot) / 10;
    const compass = document.getElementById("compasscenter")!;
    //compass.style.transform = `rotateZ(${-Math.atan2(this.magx, this.magy) * 180 / Math.PI + 90}deg)`;
    compass.style.transform = `rotateZ(${-roll}deg)`;
  }
}
