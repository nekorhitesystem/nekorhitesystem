import { Component } from '@angular/core';
import { ControlBase } from '../control.base';
import { SensorData } from '../sensor.service';

@Component({
  selector: 'angularvec-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class AngularControl extends ControlBase {
  data?: SensorData;

  onNext(data: SensorData) {
    this.data = data;
  }
}
