﻿using Microsoft.Extensions.DependencyInjection;
using NekorhiteCore.SensorModule;

namespace NekorhiteCore.SensorModule.MagicOnion
{
	public static partial class Extensions
	{

		public static IServiceCollection UseGrpcSensor(this IServiceCollection service)
		{
			return service
				.AddSingleton<SensorService>()
				.AddTransient<ISensorModuleFactory, MagicOnionSensorModuleFactory>();
		}
	}
}
