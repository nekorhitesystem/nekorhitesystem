﻿using Grpc.Core;
using Grpc.Net.Client;
using MagicOnion.Client;
using NekorhiteCore.SensorModule.Models;
using System.Reactive.Linq;

namespace NekorhiteCore.SensorModule.MagicOnion
{
    internal class MagicOnionSensorModule : ISensorModule, IDisposable
    {
        private readonly GrpcChannel channel;
        private readonly ISensorModuleService client;

        private event EventHandler<SensorData>? DataReceived;

        public MagicOnionSensorModule(string portName)
        {
            channel = GrpcChannel.ForAddress(portName, new() { Credentials = ChannelCredentials.Insecure });
            client = MagicOnionClient.Create<ISensorModuleService>(channel);
        }

        public IObservable<Models.SensorData> GetObservable()
        {
            var source = new CancellationTokenSource();
            Task.Factory.StartNew(ReceiveData, source.Token, TaskCreationOptions.LongRunning);
            return Observable.FromEventPattern<SensorData>(x => DataReceived += x, x => DataReceived -= x)
                .Select(x => x.EventArgs)
                .Select(ToModels)
                .Prepend(Models.SensorData.Default with { Status = State.Found })
                .Finally(source.Cancel)
                .Finally(source.Dispose);
        }

        private async Task ReceiveData(object? cancellationToken)
        {
            using var streaming = await client.StartSensorStreaming();
            var reader = streaming.ResponseStream;
            while (await reader.MoveNext((CancellationToken?)cancellationToken ?? CancellationToken.None))
            {
                DataReceived?.Invoke(this, reader.Current);
            }
        }

        private Models.SensorData ToModels(SensorData data)
        {
            var temp = data.Status.Temperture;
            var acc = data.Motion.Accelerometer;
            var rot = data.Motion.Gyroscope;
            var mag = data.Motion.Magnetometer;
            return Models.SensorData.Default with
            {
                Status = State.Receive,
                Speed = new("km/h", data.Status.Speed),
                Tacho = new("rpm", data.Status.Tacho),
                Tempertures = new("°C", temp.T1, temp.T2, temp.T3, temp.T4),
                Accel = new(acc.X, acc.Y, acc.Z),
                Angle = new(rot.X, rot.Y, rot.Z),
                Magnet = new(mag.X, mag.Y, mag.Z),
                Rotate = new(data.Motion.Compass),
                Voltage = new(data.Status.Voltage),
            };
        }

        public void Dispose()
        {
            channel.Dispose();
        }
    }
}
