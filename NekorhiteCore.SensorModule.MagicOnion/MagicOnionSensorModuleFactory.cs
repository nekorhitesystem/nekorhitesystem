﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.SensorModule.MagicOnion
{
    internal class MagicOnionSensorModuleFactory : ISensorModuleFactory
    {
        public MagicOnionSensorModuleFactory()
        {
        }

        public ISensorModule CreateModule(string portName)
        {
            return new MagicOnionSensorModule(portName);
        }
    }
}
