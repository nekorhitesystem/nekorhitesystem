﻿using MagicOnion;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.MachineStatus.Services
{
    public interface IStatusModuleService : IService<IStatusModuleService>
    {
        UnaryResult<double> GetCpuPercentAsync();
        UnaryResult<double> GetMemoryPercentAsync();
    }

    [MessagePackObject(true)]
    public record StatusData(double CpuAverage, double MemoryUsed);
}
