﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.MachineStatus
{
    public interface IStatusService
    {
        Task<StatusModel> GetStatusAsync();
    }

    public record StatusModel(double CpuPercent, double MemPercent);
}
