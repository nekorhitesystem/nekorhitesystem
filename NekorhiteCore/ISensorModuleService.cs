﻿using Grpc.Core;
using MagicOnion;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore
{
    public interface ISensorModuleService : IService<ISensorModuleService>
    {

        Task<ServerStreamingResult<SensorData>> StartSensorStreaming();

        UnaryResult<SensorData> SendMotionDataAsync(MotionData motion);

        UnaryResult<SensorData> SendStatusAsync(VehicleStatus status);

        UnaryResult<SensorData> SendAccelerometerAsync(Vec3d vector);

        UnaryResult<SensorData> SendGyroscopeAsync(Vec3d vector);

        UnaryResult<SensorData> SendMagnetometerAsync(Vec3d vector);

        UnaryResult<SensorData> SendCompassAsync(double rotate);

        UnaryResult<SensorSettingData> GetSettingsAsync();
    }

    [MessagePackObject(true)]
    public record SensorData(MotionData Motion, VehicleStatus Status);

    [MessagePackObject(true)]
    public record MotionData(Vec3d Accelerometer, Vec3d Gyroscope, Vec3d Magnetometer, double Compass);

    [MessagePackObject(true)]
    public record VehicleStatus(Temp Temperture, double Speed, double Tacho, double Voltage);

    [MessagePackObject(true)]
    public record Vec3d(double X, double Y, double Z);

    [MessagePackObject(true)]
    public record Temp(double T1, double T2, double T3, double T4);

    [MessagePackObject(true)]
    public record SensorSettingData(bool Accelerometer, bool Gyroscope, bool Magnetometer, bool Compass);
}
