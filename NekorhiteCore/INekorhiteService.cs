﻿using MagicOnion;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.Messages
{
    public interface INekorhiteService : IService<INekorhiteService>
    {

        UnaryResult<NekorhiteSettings> GetSettingsAsync();

        UnaryResult<NekorhiteSettings> SetSettingsAsync(NekorhiteSettings settings);
    }

    [MessagePackObject(true)]
    public record NekorhiteSettings(string? Title, double MagX, double MagY, double MagZ);
}
