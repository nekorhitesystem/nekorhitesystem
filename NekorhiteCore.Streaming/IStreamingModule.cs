﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.VideoStreaming
{
    public interface IStreamingModule
    {
        Task UpdateAsync(Stream stream);
    }
}
