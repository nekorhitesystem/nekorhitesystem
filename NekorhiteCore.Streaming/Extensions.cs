﻿using Microsoft.Extensions.DependencyInjection;
namespace NekorhiteCore.VideoStreaming
{
    public static class Extensions
	{
		public static IServiceCollection EnableStreamingService(this IServiceCollection service)
		{
			return service.AddSingleton<StreamingService>();
		}
	}
}
