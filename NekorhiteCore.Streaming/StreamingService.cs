﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.VideoStreaming
{
    public class StreamingService
    {
        private readonly IStreamingModuleFactory factory;
        private readonly ConcurrentDictionary<string, IStreamingModule> modules = new();

        private static readonly object lockobj = new();

        public StreamingService(IStreamingModuleFactory factory)
        {
            this.factory = factory;
        }

        public IStreamingModule GetOrCreateFrontCamera()
        {
            lock (lockobj)
            {
                return modules.GetOrAdd("front", factory.CreateModule);
            }
        }
    }
}
