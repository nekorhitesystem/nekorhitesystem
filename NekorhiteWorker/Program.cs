using Grpc.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting.WindowsServices;
using NekorhiteCore.SensorModule;
using NekorhiteCore.SensorModule;
using NekorhiteCore.SensorModule.SerialPort;
using NekorhiteWorker.Configurations;
using NekorhiteWorker.Mappers;
using NekorhiteWorker.Mappers.Repositories;
using NekorhiteWorker.Services;
using System.Net;


var builder = WebApplication.CreateBuilder(new WebApplicationOptions
{
    Args = args,
    ContentRootPath = WindowsServiceHelpers.IsWindowsService() ? AppContext.BaseDirectory : default
});

builder.Host.UseWindowsService(options => options.ServiceName = "NekorhiteWorker");

builder.Services.Configure<SensorSettings>(builder.Configuration.GetSection("SensorSettings"));
builder.Services.Configure<ServerSettings>(builder.Configuration.GetSection("ServerSettings"));

var connection = builder.Configuration.GetConnectionString("ApplicationDbContext");

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(connection));
builder.Services.AddTransient<IApplicationRepository, ApplicationDbContext>();

builder.Services.AddHostedService<BloadcastWatcherService>();
builder.Services.AddHostedService<SensorReceiveService>();
builder.Services.UseSensor();
builder.Services.AddGrpc();
builder.Services.AddMagicOnion();

var app = builder.Build();

using var scope = app.Services.CreateScope();
using var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
if (context is not null)
{
    await context.Database.MigrateAsync();
}

//new Server()
//{
//    Ports = { new(IPAddress.Any.ToString(), 7000, ServerCredentials.Insecure) },
//    Services = { Sensor.BindService(new SensorService()) }
//}.Start();
//app.MapGrpcService<SensorConnectionService>();
app.MapMagicOnionService();
//app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
await app.RunAsync();