﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using NekorhiteWorker.Configurations;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteWorker.Services
{
    internal class BloadcastWatcherService : BackgroundService
    {
        private readonly ILogger<BloadcastWatcherService> logger;
        private readonly ServerSettings settings;

        private const string keyword = "#nekorider by kameme1208#";

        public BloadcastWatcherService(ILogger<BloadcastWatcherService> logger, IOptions<ServerSettings> settings)
        {
            this.settings = settings.Value;
            this.logger = logger;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("ブロードキャストメッセージ監視サービスを起動しました。");
            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("ブロードキャストメッセージ監視サービスを停止しました。");
            return base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    using var client = new UdpClient(new IPEndPoint(IPAddress.Any, 0))
                    {
                        EnableBroadcast = true,
                        MulticastLoopback = false,
                    };
                    while (!stoppingToken.IsCancellationRequested)
                    {
                        var endpoint = new IPEndPoint(IPAddress.Broadcast, settings.BloadcastPort);
                        await client.SendAsync(keyword.Encrypt(), endpoint, stoppingToken);
                        await Task.Delay(1000, stoppingToken);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "ブロードキャストメッセージで実行時エラーが発生しました。");
                }

            }
        }
    }

    static class Cryptor
    {
        public static ReadOnlyMemory<byte> Encrypt(this string datastring, string password = "nekopass")
        {
            using var aes = Aes.Create();
            using var memory = new MemoryStream();
            using var writer = new BinaryWriter(memory);
            using var deriver = new Rfc2898DeriveBytes(password, 32);
            writer.Write(deriver.Salt);
            writer.Write(aes.IV);
            aes.Key = deriver.GetBytes(32);
            using var stream = new CryptoStream(memory, aes.CreateEncryptor(), CryptoStreamMode.Write);
            using var binary = new BinaryWriter(stream);
            binary.Write(datastring);
            stream.FlushFinalBlock();
            return memory.GetBuffer().AsMemory(0..(int)memory.Position);
        }

        public static string Decrypt(this byte[] databytes, string password = "nekopass")
        {
            using var aes = Aes.Create();
            using var memory = new MemoryStream(databytes);
            using var reader = new BinaryReader(memory);
            using var deriver = new Rfc2898DeriveBytes(password, reader.ReadBytes(32));
            aes.IV = reader.ReadBytes(aes.IV.Length);
            aes.Key = deriver.GetBytes(32);
            using var stream = new CryptoStream(memory, aes.CreateDecryptor(), CryptoStreamMode.Read);
            using var binary = new BinaryReader(stream);
            return binary.ReadString();
        }
    }
}
