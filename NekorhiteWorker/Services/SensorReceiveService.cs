﻿using Grpc.Core;
using Grpc.Net.Client;
using MagicOnion.Client;
using Microsoft.Extensions.Options;
using NekorhiteCore;
using NekorhiteCore.SensorModule;
using NekorhiteWorker.Configurations;

namespace NekorhiteWorker.Services
{
    public class SensorReceiveService : BackgroundService
    {
        private readonly ILoggerFactory logger;
        private readonly SensorSettings settings;
        private readonly SensorService service;

        public SensorReceiveService(ILoggerFactory logger, IOptions<SensorSettings> settings, SensorService service)
        {
            this.settings = settings.Value;
            this.logger = logger;
            this.service = service;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            logger.CreateLogger<SensorReceiveService>().LogInformation("車載センサー監視サービスを起動しました。");
            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            logger.CreateLogger<SensorReceiveService>().LogInformation("車載センサー監視サービスを停止しました。");
            return base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                using var channel = GrpcChannel.ForAddress("http://localhost:65000", new() { Credentials = ChannelCredentials.Insecure });
                var client = MagicOnionClient.Create<ISensorModuleService>(channel);
                var sensor = service.GetOrCreate(settings.PortName);
                using var subscribe = sensor.GetObservable().Subscribe(data => SendData(client, data));

                while (!stoppingToken.IsCancellationRequested)
                {
                    await Task.Delay(1000, stoppingToken);
                }
            }
            catch
            {

            }
        }

        private void SendData(ISensorModuleService client, NekorhiteCore.SensorModule.Models.SensorData data)
        {
            if (settings.Acceleration)
            {
                client.SendAccelerometerAsync(new(data.Accel.X, data.Accel.Y, data.Accel.Z));
            }
            if (settings.Gyroscope)
            {
                client.SendGyroscopeAsync(new(data.Angle.X, data.Angle.Y, data.Angle.Z));
            }
            if (settings.Magnetism)
            {
                client.SendMagnetometerAsync(new(data.Magnet.X, data.Magnet.Y, data.Magnet.Z));
            }
            if (settings.Compass)
            {
                client.SendCompassAsync(Math.Atan2(data.Magnet.X, data.Magnet.Y) * 180 / Math.PI);
            }
            var temp = data.Tempertures;
            client.SendStatusAsync(new(new(temp.Temp1, temp.Temp2, temp.Temp3, temp.Temp4), data.Speed.Value, data.Tacho.Value, data.Voltage.V1));
        }
    }
}
