﻿using Grpc.Core;
using MagicOnion;
using MagicOnion.Server;
using Microsoft.Extensions.Options;
using NekorhiteCore;
using NekorhiteCore.Messages;
using NekorhiteWorker.Configurations;
using NekorhiteWorker.Mappers.Repositories;
using NekorhiteWorker.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteWorker.Services
{
    public class SensorAggregateService : ServiceBase<ISensorModuleService>, ISensorModuleService
    {
        private static event EventHandler? SensorUpdateEvent;

        private static Vec3d acc = new(0, 0, 0);
        private static Vec3d mag = new(0, 0, 0);
        private static Vec3d rot = new(0, 0, 0);
        private static double compass;

        private static Temp temp = new(0, 0, 0, 0);
        private static double speed;
        private static double tacho;
        private static double voltage;

        private static SensorData SensorData => new(new(acc, rot, mag, compass), new(temp, speed, tacho, voltage));

        private readonly SensorSettings settings;

        private readonly IApplicationRepository repository;

        public SensorAggregateService(IOptions<SensorSettings> settings, IApplicationRepository repository)
        {
            this.settings = settings.Value;
            this.repository = repository;
        }

        public async Task<ServerStreamingResult<SensorData>> StartSensorStreaming()
        {
            var context = GetServerStreamingContext<SensorData>();
            var observable = Observable.FromEventPattern(x => SensorUpdateEvent += x, x => SensorUpdateEvent -= x);
            using var subscriver = observable.Subscribe();
            foreach (var data in observable.Select(x => SensorData))
            {
                await context.WriteAsync(data);
            }
            return context.Result();
        }

        public UnaryResult<SensorData> SendMotionDataAsync(MotionData motion)
        {
            acc = motion.Accelerometer;
            mag = motion.Magnetometer;
            rot = motion.Gyroscope;
            compass = motion.Compass;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorData> SendStatusAsync(VehicleStatus status)
        {
            temp = status.Temperture;
            speed = status.Speed;
            tacho = status.Tacho;
            voltage = status.Voltage;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorData> SendAccelerometerAsync(Vec3d vector)
        {
            acc = vector;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorData> SendGyroscopeAsync(Vec3d vector)
        {
            rot = vector;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorData> SendMagnetometerAsync(Vec3d vector)
        {
            mag = vector;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorData> SendCompassAsync(double rotate)
        {
            compass = rotate;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorSettingData> GetSettingsAsync()
        {
            var ret = settings;
            return UnaryResult(new SensorSettingData(ret.Acceleration, ret.Gyroscope, ret.Magnetism, ret.Compass));
        }

    }
}
