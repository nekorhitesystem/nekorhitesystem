﻿/*
using Grpc.Core;
using MagicOnion;
using MagicOnion.Server;
using NekorhiteCore.MachineStatus.Services;
using NekorhiteCore.Messages;
using System.Diagnostics;
using System.Management;
namespace NekorhiteWorker.Services
{
    public class StatusModuleService2 : ServiceBase<IStatusModuleService>, IStatusModuleService
    {
        private static readonly PerformanceCounter cpu;
        private static readonly PerformanceCounter mem;
    
        static StatusModuleService2()
        {
            cpu = new PerformanceCounter("Processor", "% Processor Time", "_Total");
            mem = new PerformanceCounter("Memory", "Available MBytes");
            cpu.NextValue();
            mem.NextValue();
        }
    
        public UnaryResult<double> GetCpuPercentAsync()
        {
            return UnaryResult((double)cpu.NextValue());
        }
    
        public UnaryResult<double> GetMemoryPercentAsync()
        {
            return UnaryResult((4096.0 - mem.NextValue()) / 4096.0 * 100);
        }
    }
}
*/