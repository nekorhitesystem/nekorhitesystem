﻿using MagicOnion;
using MagicOnion.Server;
using NekorhiteCore.Messages;
using NekorhiteWorker.Mappers.Repositories;
using NekorhiteWorker.Models;

namespace NekorhiteWorker.Services
{
    public class NekorhiteService : ServiceBase<INekorhiteService>, INekorhiteService
    {
        private readonly IApplicationRepository repository;

        public NekorhiteService(IApplicationRepository repository)
        {
            this.repository = repository;
        }

        public async UnaryResult<NekorhiteSettings> GetSettingsAsync()
        {            
            var settings = await repository.GetSettingsAsync();
            return new (settings.Title, settings.MagX, settings.MagY, settings.MagZ);
        }

        public async UnaryResult<NekorhiteSettings> SetSettingsAsync(NekorhiteSettings settings)
        {
            var convertion = new ConvertionNekorhiteSetting(settings);
            await repository.SetSettingsAsync(convertion);
            return convertion;
        }

        private record ConvertionNekorhiteSetting: NekorhiteSettings, INekorhiteSettings
        {
            public ConvertionNekorhiteSetting(NekorhiteSettings neko) : base(neko.Title, neko.MagX, neko.MagY, neko.MagZ) { }
        }
    }
}
