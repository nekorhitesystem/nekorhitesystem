﻿using Grpc.Core;
using MagicOnion;
using MagicOnion.Server;
using NekorhiteCore.MachineStatus.Services;
using NekorhiteCore.Messages;
using System.Diagnostics;
using System.Management;

namespace NekorhiteWorker.Services
{
    public class StatusModuleService : ServiceBase<IStatusModuleService>, IStatusModuleService
    {
        private static readonly ManagementObjectSearcher? cpu = null;
        private static readonly ManagementObjectSearcher? mem = null;
    
        static StatusModuleService()
        {
            try
            {
                var scope = new ManagementScope("\\\\.\\ROOT\\cimv2");
                var query1 = new ObjectQuery("SELECT * FROM Win32_PerfFormattedData_PerfOS_Processor");
                var query2 = new ObjectQuery("SELECT * FROM Win32_PerfFormattedData_PerfOS_Memory");
                cpu = new ManagementObjectSearcher(scope, query1);
                mem = new ManagementObjectSearcher(scope, query2);
            }
            catch
            {
    
            }
        }
    
        public UnaryResult<double> GetCpuPercentAsync()
        {
            var cpuobj = cpu?.Get().OfType<ManagementObject>().First(x => $"{x["Name"]}" == "_Total");
            var cpuval = cpuobj is not null ? (ulong)cpuobj["PercentProcessorTime"] : 0;
            return UnaryResult((double)cpuval);
        }
    
        public UnaryResult<double> GetMemoryPercentAsync()
        {
            var memobj = mem?.Get().OfType<ManagementObject>().First();
            var memval = memobj is not null ? (uint)memobj["PercentCommittedBytesInUse"] : 0;
            return UnaryResult((double)memval);
        }
    }
}
