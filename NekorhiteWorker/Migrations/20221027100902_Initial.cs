﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NekorhiteWorker.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Settings",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "TEXT", nullable: false),
                    Title = table.Column<string>(type: "TEXT", nullable: true),
                    Calib_MagnetX = table.Column<double>(type: "REAL", nullable: false),
                    Calib_MagnetY = table.Column<double>(type: "REAL", nullable: false),
                    Calib_MagnetZ = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Settings", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Settings");
        }
    }
}
