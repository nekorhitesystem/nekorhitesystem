﻿namespace NekorhiteWorker.Models
{
    public interface INekorhiteSettings
    {
        string? Title { get; }

        double MagX { get; }

        double MagY { get; }

        double MagZ { get; }
    }
}
