﻿using NekorhiteWorker.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteWorker.Mappers.Repositories
{
    public interface IApplicationRepository
    {
        Task<INekorhiteSettings> GetSettingsAsync();

        Task SetSettingsAsync(INekorhiteSettings settings);
    }
}
