﻿using Microsoft.EntityFrameworkCore;
using NekorhiteWorker.Mappers.Entities;
using NekorhiteWorker.Mappers.Repositories;
using NekorhiteWorker.Models;

namespace NekorhiteWorker.Mappers
{
    public class ApplicationDbContext : DbContext, IApplicationRepository
    {
        private DbSet<SettingsData>? Settings { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<SettingsData>().HasKey(x => new { x.Id });
        }

        public async Task<INekorhiteSettings> GetSettingsAsync()
        {
            var settings = Settings ?? throw new NullReferenceException(nameof(Settings));
            var data = await settings.FirstOrDefaultAsync();
            if (data is null)
            {
                await settings.AddAsync(data = new SettingsData());
                await SaveChangesAsync();
            }
            return data;
        }

        public async Task SetSettingsAsync(INekorhiteSettings data)
        {
            var settings = Settings ?? throw new NullReferenceException(nameof(Settings));
            var dbdata = await settings.FirstOrDefaultAsync();
            if (dbdata is null)
            {
                await settings.AddAsync(dbdata = new SettingsData());
            }
            dbdata.Title = data.Title;
            dbdata.MagX = data.MagX;
            dbdata.MagY = data.MagY;
            dbdata.MagZ = data.MagZ;
            await SaveChangesAsync();
        }
    }
}
